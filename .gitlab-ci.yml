stages:
  - prebuild
  - builds

.multilib: &multilib |
  dpkg_architecture=$(which dpkg-architecture 2>/dev/null || true)
  rpm=$(which rpm 2>/dev/null || true)
  if test -x "$dpkg_architecture"; then
    multilib=$(dpkg-architecture -q DEB_TARGET_MULTIARCH);
  elif test -x "$rpm"; then
    multilib=$(rpm --eval '%{_lib}');
  fi

.native-environment: &native-environment |
  export VIRT_PREFIX="$HOME/build/libvirt"
  export PATH="$VIRT_PREFIX/bin:$PATH"
  export C_INCLUDE_PATH="$VIRT_PREFIX/include"
  export LD_LIBRARY_PATH="$VIRT_PREFIX/$multilib"
  export PKG_CONFIG_PATH="$VIRT_PREFIX/$multilib/pkgconfig"
  export XDG_DATA_DIRS="$VIRT_PREFIX/share:/usr/share:/usr/local/share"
  export GI_TYPELIB_PATH="$VIRT_PREFIX/$multilib/girepository-1.0"
  export OSINFO_SYSTEM_DIR="$VIRT_PREFIX/share/osinfo"
  export MAKEFLAGS="-j $(getconf _NPROCESSORS_ONLN)"
  export CCACHE_BASEDIR="$(pwd)"
  export CCACHE_DIR="$CCACHE_BASEDIR/ccache"
  export CCACHE_MAXSIZE="50M"
  export PATH="$CCACHE_WRAPPERSDIR:$PATH"

.osinfo-db-tools-build: &osinfo-db-tools-build |
  pushd /tmp/
  git clone https://gitlab.com/libosinfo/osinfo-db-tools.git
  cd osinfo-db-tools
  mkdir build
  cd build
  meson .. . --prefix=$VIRT_PREFIX
  $NINJA install
  popd

.osinfo-db-build: &osinfo-db-build |
  pushd .
  mkdir build
  cd build
  $MAKE -f ../Makefile VPATH=..
  $MAKE -f ../Makefile VPATH=.. install OSINFO_DB_TARGET="--system"
  popd

.osinfo-db-check: &osinfo-db-check |
  pushd .
  cd build
  $MAKE -f ../Makefile VPATH=.. check
  popd

.osinfo-db-rpm: &osinfo-db-rpm |
  if test "$RPM" = "skip"; then
    exit 0
  fi
  pushd .
  cd build
  sed -i -e 's/BuildRequires: *osinfo-db.*//' *.spec*
  rpmbuild --clean --define "_topdir `pwd`/rpmbuild" --define "_sourcedir `pwd`" -ba osinfo-db.spec
  popd

.native-build-job: &native-build-job
  stage: builds
  image: quay.io/libvirt/buildenv-libosinfo-$NAME:latest
  cache:
    paths:
      - ccache
    key: "$CI_JOB_NAME"
  script:
    - *multilib
    - *native-environment
    - *osinfo-db-tools-build
    - *osinfo-db-build
    - *osinfo-db-check
    - *osinfo-db-rpm

# Check that all commits are signed-off for the DCO.
# Skip on "libosinfo" namespace, since we only need to run
# this test on developer's personal forks from which
# merge requests are submitted
check-dco:
  stage: prebuild
  image: registry.gitlab.com/libvirt/libvirt-ci/check-dco:master
  script:
    - /check-dco libosinfo
  except:
    variables:
      - $CI_PROJECT_NAMESPACE == "libosinfo"

centos-7:
  <<: *native-build-job
  variables:
    NAME: centos-7

centos-8:
  <<: *native-build-job
  variables:
    NAME: centos-8

debian-9:
  <<: *native-build-job
  variables:
    NAME: debian-9
    RPM: skip

debian-10:
  <<: *native-build-job
  variables:
    NAME: debian-10
    RPM: skip

debian-sid:
  <<: *native-build-job
  variables:
    NAME: debian-sid
    RPM: skip

fedora-31:
  <<: *native-build-job
  variables:
    NAME: fedora-31

fedora-32:
  <<: *native-build-job
  variables:
    NAME: fedora-32

fedora-rawhide:
  <<: *native-build-job
  variables:
    NAME: fedora-rawhide

opensuse-151:
  <<: *native-build-job
  variables:
    NAME: opensuse-151

ubuntu-1804:
  <<: *native-build-job
  variables:
    NAME: ubuntu-1804
    RPM: skip

ubuntu-2004:
  <<: *native-build-job
  variables:
    NAME: ubuntu-2004
    RPM: skip
